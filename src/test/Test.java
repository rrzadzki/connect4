package test;

import core.Board;

public class Test {
    public static void main(String[] args) {
        testStraightWin();
        testMiddleWin();
        testFullColumn();
        testInvalidColumns();
        testDrawCondition();
    }

    private static void testStraightWin() {
        Board board = new Board();
        assert board.isPlayable();
        board.insertAt(board.PLAYER1,0);
        board.insertAt(board.PLAYER1,0);
        board.insertAt(board.PLAYER1,0);
        assert board.getWinner() == board.NONE;
        board.insertAt(board.PLAYER1,0);
        assert  board.getWinner() == board.PLAYER1;
        assert !board.isPlayable();
    }

    /**
     * When the column is full, it should reject new moves.
     */
    private static void testFullColumn() {
        Board board = new Board();
        board.insertAt(board.PLAYER1, 4);
        board.insertAt(board.PLAYER1, 4);
        board.insertAt(board.PLAYER1, 4);
        board.insertAt(board.PLAYER1, 4);
        board.insertAt(board.PLAYER1, 4);
        board.insertAt(board.PLAYER1, 4);
        assert board.insertAt(board.PLAYER1, 4) == -1;
    }

    /**
     * When a column out of bounds is chosen, it should be rejected.
     */
    private static void testInvalidColumns() {
        Board board = new Board();
        assert board.insertAt(board.PLAYER1, -1) == -1;
        assert board.insertAt(board.PLAYER2, 35) == -1;
    }

    /**
     *  If the board is filled without a four-in-a-row, it should result in a draw.
     */
    private static void testDrawCondition() {
        Board board = new Board();
        for(int c=0; c<7; c++) {
            char p = c % 2 == 0 ? board.PLAYER1 : board.PLAYER2;
            for(int d=0; d<3; d++) {
                assert board.insertAt(p,c) != -1;
            }
        }
        for(int c=0; c<7; c++) {
            char p = c % 2 != 0 ? board.PLAYER1 : board.PLAYER2;
            for(int d=0; d<3; d++) {
                assert board.insertAt(p,c) != -1;
            }
        }
        assert board.isPlayable() == false;
    }

    /**
     * Ensure a win is recorded if a piece is dropped in the middle of a sequence.
     */
    private static void testMiddleWin() {
        Board board = new Board();
        board.insertAt('X',2);
        board.insertAt('X',3);
        board.insertAt('X',5);
        board.insertAt('X',4);
        assert board.isPlayable() == false;
        assert board.getWinner() == 'X';
    }
}
